<x-app-layout>
    <form action="{{route('profesion.update',$profesion->id)}}" method="POST">
        @csrf
        @method('PUT')
        <div>
            <label for="nombres">Nombres</label>
            <input type="text" name="nombres" value={{$profesion->nombre}} >
        </div>

        <input type="submit" value="Actualizar">
    </form>
</x-app-layout>
