<x-app-layout>
    <form action="{{route('user.update',$users->id)}}" method="POST">
        @csrf
        @method('PUT')
        <div>
            <label for="nombres">Nombres</label>
            <input type="text" name="nombres" value={{$users->name}} >
        </div>
        <div>
            <label for="correo_electronico">Correo Electrónico</label>
            <input type="email" name="correo_electronico" value={{$users->email}} >
        </div>

        <input type="submit" value="Actualizar">
    </form>
</x-app-layout>
