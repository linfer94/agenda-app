<x-app-layout>

    <form action="{{route('user.store')}}" method="POST">
        @csrf
        <div>
            <label for="nombres">Nombres</label>
            <input type="text" name="nombres">
        </div>
        <div>
            <label for="correo_electronico">Correo Electrónico</label>
            <input type="email" name="correo_electronico">
        </div>

        <input type="submit" value="Registrar">
    </form>
</x-app-layout>
