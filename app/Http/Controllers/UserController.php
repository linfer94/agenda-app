<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $usuarios = User::all();

        return view('usuarios.index', [
            'usuarios'=>$usuarios
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $users = User::create($input);
        return view('user.show', [
            'users'=> $users
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(User $users)
    {
        return view('usuarios.show',[
            'users'=>$users
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $users)
    {
        return view('usuarios.edit',[
            'user'=>$users
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $users)
    {
        $input = $request->all();
        $users->update($input);
        return view('usuarios.show', [
            'users'=>$users
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $profesion)
    {
        //
    }
}
