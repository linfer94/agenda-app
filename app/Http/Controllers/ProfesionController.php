<?php

namespace App\Http\Controllers;

use App\Models\Profesion;
use Illuminate\Http\Request;

class ProfesionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $profesiones = Profesion::all();

        return view('profesiones.index', [
            'profesiones' => $profesiones
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('profesiones.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $profesion = Profesion::create($input);
        return view('profesiones.show',[
            'profesion' => $profesion
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Profesion $profesion)
    {
        return view('profesiones.show', [
            'profesion'=>$profesion
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Profesion $profesion)
    {
        return view('profesiones.edit', [
            'profesion'=>$profesion
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Profesion $profesion)
    {
        $input = $request->all();
        $profesion->update($input);
        return view('profesiones.show', [
            'profesion'=> $profesion
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Profesion $profesion)
    {
        //
    }
}
